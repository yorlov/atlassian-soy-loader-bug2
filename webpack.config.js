const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {LoaderOptionsPlugin} = require('webpack');

const
    resolve = (...dir) => path.resolve(__dirname, ...dir),
    frontend = (...dir) => resolve('src', ...dir),
    distPath = resolve('dist');

module.exports = {
    entry: frontend('index.js'),
    output: {
        path: distPath,
    },
    plugins: [
        new LoaderOptionsPlugin({
            debug: true,
        }),
        new HtmlWebpackPlugin({
            templateContent: `
                <!DOCTYPE html>
                <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <script src="/soyutils.min.js"></script>
                        <title>WE</title>
                    </head>
                    <body>
                    <div id="app"></div>
                    </body>
                </html>
            `
        })
    ],
    module: {
        rules: [
            {
                test: /\.soy/,
                use: [
                    {
                        loader: '@atlassian/soy-loader',
                        options: {
                            customModuleMap: {
                                'aui': frontend('aui', 'aui.soy'),
                                'aui.message': frontend('aui', 'message.soy')
                            },
                            modularize: true,
                            expose: false
                        }
                    },
                ],
            },
        ]
    },
    devServer: {
        historyApiFallback: true,
        contentBase: distPath
    },
    stats: 'errors-warnings'
};